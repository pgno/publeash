import matter from "tiny-matter";
import { marked } from "marked";

function walk(node,urls,level = 0) {
    if (Array.isArray(node)) {
        node.forEach((n) => {
        if (n.type === "list") {
            level ++;
            n.items.forEach((i) => {
            walk(i,urls,level);
            });
        } else {
            if (n.type === "text") {
            n.tokens.forEach((i) => {
                walk(i,urls,level);
            });
            }
        }
        });
    } else {
        if (node.type === "list_item") {
        walk(node.tokens,urls,level);
        }

        if (node.type === "link") {
        urls.push({ label : node.text , url :  node.href, level});
        }
    }
}

async function getContent(url) {
    const page = await fetch(url+"/download")
    const pageMD = await page.text();
    const pageData = matter(pageMD);
    const content = marked(pageData.content)
    return content;
}

export const extractSiteContent = async function (url) {
  
    const page = await fetch(url+"/download");
    const markdown = await page.text();
    const postdata = matter(markdown);
    const title = postdata.data.title;
    const nav = marked.lexer(postdata.content);
    const items = []
    walk (nav,items);

    for (let item of items) {
        const content = await getContent(item.url)
        item.content = content
    }
    return {nav,items,title};    
};

