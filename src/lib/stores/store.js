import { writable } from 'svelte/store';

function createAppStore(){
    const store = writable({
        pageUrl : '',
        items : []
    });
    return store;
}

export const appStore = createAppStore();
export const mainUrl = writable('');
export const print = writable(false);