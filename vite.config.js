import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import preprocess from 'svelte-preprocess';

// https://vitejs.dev/config/
export default defineConfig({
  base : '/publeash',
  publicDir : './static',
  build: { 
    outDir: './public'    
  },
  plugins: [
    svelte({
      preprocess: preprocess()
    })
  ],
})
