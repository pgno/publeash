# Publeash

## Expérimentation de plateforme de publication "pad2web2print".

L'interface permet de fournir l'url d'un pad [Hedgedoc](https://hedgedoc.org/) structuré sous la forme d'une liste d'url pointant vers d'autres pads. A partir de ce "menu", on obtient un mini-site web. Il aussi est possible de générer un PDF.

## Structure du pad principal

Le pad dont on fournit l'url doit contenir uniquement une liste de lien vers d'autres pads.

Exemple : publication d'Alice au pays des merveilles 

```
---
title: Alice’s Adventures in Wonderland
author: Lewis Carroll
---
- [Down the Rabbit-Hole](https://pad.lescommuns.org/alice-in-wonderland-chapter1)
- [The Pool of Tears](https://pad.lescommuns.org/alice-in-wonderland-chapter2)
- [A Caucus-Race and a Long Tale](https://pad.lescommuns.org/alice-in-wonderland-chapter3)
- [The Rabbit Sends in a Little Bill](https://pad.lescommuns.org/alice-in-wonderland-chapter4)
- [Advice from a Caterpillar](https://pad.lescommuns.org/alice-in-wonderland-chapter5)
- [Pig and Pepper](https://pad.lescommuns.org/alice-in-wonderland-chapter6)
- [A Mad Tea-Party](https://pad.lescommuns.org/alice-in-wonderland-chapter7)
- [The Queen’s Croquet-Ground](https://pad.lescommuns.org/alice-in-wonderland-chapter8)
- [The Mock Turtle’s Story](https://pad.lescommuns.org/alice-in-wonderland-chapter9)
- [The Lobster Quadrille](https://pad.lescommuns.org/alice-in-wonderland-chapter10)
- [Who Stole the Tarts?](https://pad.lescommuns.org/alice-in-wonderland-chapter11)
- [Alice’s Evidence](https://pad.lescommuns.org/alice-in-wonderland-chapter12)
```


Pour tester, aller sur https://pgno.gitlab.io/publeash/ et saisir https://pad.lescommuns.org/alice-in-wonderland dans le champ de saisie et cliquez sur Actualiser.
Les données "frontmatter" permettent de définir l'auteur et le titre qui sera utilisé pour le site et la couverture du PDF.

Il est possible de créer des sous-menus en créant des listes imbriquées.

```
- [Accueil](https://...)
- [Rubrique 1](https://...)
    - [Sous-rubrique 1.1](https://...)
        - [Sous-rubrique 1.1.1](https://...)
    - [Sous-rubrique 1.2](https://...)
    - [Sous-rubrique 1.3](https://...)
- [Rubrique 2](https://...)
    - [Sous-rubrique 2.1](https://...)
    - [Sous-rubrique 2.2](https://...)
    - [Sous-rubrique 2.3](https://...)
- [A propos](https://...)
- [Contact](https://...)
```

## Template du site

[to be done]
En passant la variable 'xxx' en en-tête du frontmatter, on peut utiliser le template xxx.

## Maquette PDF

La maquette est générée par la librairie [paged.js](https://pagedjs.org/). Une feuille de style par défaut est utilisée.

[to be done] 
Une fenêtre permet d'accéder à un éditeur de style pour personnaliser le PDF ?

## Environnement technique

L'interface est utilisable via : https://pgno.gitlab.io/publeash/
Elle est développée avec le framework [Svelte](https://svelte.dev/) et hébergé sur ce dépôt via Gitlab Pages.
La conversion mardown-html est réalisée par la librairie [marked.js](https://marked.js.org/) et [tiny-matter](https://www.npmjs.com/package/tiny-matter) pour la lecture des entête frontmatter en YAML.

[to be done]
Vérifier les plugins existants pour marked.js qui permet d'obtenir le même rendu que hedgedoc (mermaid, ...)
